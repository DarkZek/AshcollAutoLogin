package ashcollauthlogin.darkzek.com.CaptivePortalSystem

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.widget.Toast
import ashcollauthlogin.darkzek.com.LoginResponse

class ScreenDetection: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val usernameText = context!!.getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("username", "DEFAULT")
        val passwordText = context.getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("password", "DEFAULT")

        val response = CaptivePortalManager.getInstance().Login(usernameText, passwordText, context)

        if (response == LoginResponse.SUCCESS) {
            Toast.makeText(context, "Logged you into the schools WiFi!", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        fun Setup(context: Context) {
            context.registerReceiver(ScreenDetection(), IntentFilter("android.intent.action.SCREEN_ON"))
        }
    }
}