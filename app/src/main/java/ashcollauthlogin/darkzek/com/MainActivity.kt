package ashcollauthlogin.darkzek.com

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardedVideoAd


import ashcollauthlogin.darkzek.com.CaptivePortalSystem.CaptivePortalManager
import ashcollauthlogin.darkzek.com.CaptivePortalSystem.ScreenDetection
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAdListener

class MainActivity : AppCompatActivity() {

    private var mAdView: AdView? = null
    private var mRewardedVideoAd: RewardedVideoAd? = null
    private var premiumButton: Button? = null


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        AshcollAutoLogin.getInstance().createNotificationChannel(this)

        //Check if its their first time opening app
        if (firstTime()) {
            loadLoginActivity()
            return
        }

        premiumButton = this.findViewById(R.id.premiumButton)

        loadAd()

        //Load the users credentials
        loadCredentials()

        //Load premium button
        detectTextChange()

        val premium = this.getSharedPreferences("credentials", 0).contains("premium")

        //Load premium settings
        if (premium) {
            ScreenDetection.Setup(this)
            premiumButton!!.visibility = View.GONE
        } else {
            mRewardedVideoAd!!.rewardedVideoAdListener = AdRewardManager(this)
            mRewardedVideoAd!!.loadAd("ca-app-pub-5689777634096933/1702403608", AdRequest.Builder().build())
            this.findViewById<TextView>(R.id.premiumText)!!.visibility = View.GONE
        }

        //Load battery optimisation settings
        if (!batteryOptimisations() || !premium) {
            this.findViewById<LinearLayout>(R.id.batteryOptimisations).visibility = View.GONE
        }
    }

    private fun firstTime(): Boolean {
        //Check if its their first time
        if (getSharedPreferences("credentials", Context.MODE_PRIVATE).contains("username")) {
            //Already setup!

            //Setup wifi listener
            AshcollAutoLogin.setupWifiListener(applicationContext)

            //For some reason android throws a bunch of random errors here
            try {
                setContentView(R.layout.activity_main)
            } catch (e: Exception) {

            }

            //Set the scroll to the top
            val v = findViewById<ScrollView>(R.id.settingsScroll)
            v.requestFocus()

            return false
        }
        return true
    }

    private fun batteryOptimisations(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false
        }

        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        return !pm.isIgnoringBatteryOptimizations(applicationContext.packageName)
    }

    private fun loadCredentials() {

        val usernameText = getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("username", "DEFAULT")
        val passwordText = getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("password", "DEFAULT")
        val timesLoggedIn = getSharedPreferences("credentials", Context.MODE_PRIVATE).getInt("timesLoggedIn", 0)

        (findViewById<View>(R.id.usernameField) as EditText).setText(usernameText)
        (findViewById<View>(R.id.passwordField) as EditText).setText(passwordText)
        (findViewById<View>(R.id.timesLoggedIn) as TextView).text = timesLoggedIn.toString() + " times"
    }

    private fun loadAd() {
        MobileAds.initialize(this, "ca-app-pub-5689777634096933~5341567652")

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this)

        mAdView!!.loadAd(adRequest)
    }

    fun showBatteryOptimisations(view: View) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return
        }

        val intent = Intent()
        intent.action = Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
        this.startActivity(intent)
    }

    private fun detectTextChange() {

        val username = findViewById<EditText>(R.id.usernameField)
        val password = findViewById<EditText>(R.id.passwordField)

        //Set the password area to a password area
        password.inputType = 129

        username.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                if (username.text.toString().equals("", ignoreCase = true)) {
                    //Invalid!
                    (findViewById<View>(R.id.usernameField) as EditText).error = "This field can not be blank!"
                    return
                }

                getSharedPreferences("credentials", Context.MODE_PRIVATE).edit().putString("username", username.text.toString()).apply()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence, start: Int, two: Int, three: Int) { }
        })

        password.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                if (password.text.toString().equals("", ignoreCase = true)) {
                    //Invalid!
                    (findViewById<View>(R.id.passwordField) as EditText).error = "This field can not be blank!"
                    return
                }

                getSharedPreferences("credentials", Context.MODE_PRIVATE).edit().putString("password", password.text.toString()).apply()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence, start: Int, two: Int, three: Int) { }
        })

    }

    //Button pressed
    fun detectCaptivePortal(view: View) {
        manuallyRun(view.context)

        val timesLoggedIn = getSharedPreferences("credentials", Context.MODE_PRIVATE).getInt("timesLoggedIn", 0)
        (findViewById<View>(R.id.timesLoggedIn) as TextView).text = "$timesLoggedIn times"
    }


    private fun loadLoginActivity() {
        val myIntent = Intent(this, IntroActivity::class.java)
        startActivity(myIntent)

        finish()
    }

    private fun manuallyRun(context: Context) {
        val usernameText = getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("username", "DEFAULT")
        val passwordText = getSharedPreferences("credentials", Context.MODE_PRIVATE).getString("password", "DEFAULT")

        //Login
        val response = CaptivePortalManager.getInstance().Login(usernameText, passwordText, this)

        when (response) {
            LoginResponse.SUCCESS -> {
                notifyUser("Success!", "Successfully logged into the Student_BYOD", context)
            }
            LoginResponse.NO_INTERNET -> {
                notifyUser("Error!", "You must be connected to the schools WiFi for this app to work!", context)
            }
            LoginResponse.LOGIN_PAGE_UNACCESSABLE -> {
                notifyUser("Error!", "Couldn't access the login page! Try toggling airplane mode on then off again", context)
            }
            LoginResponse.UNKNOWN_ERROR -> {
                val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://detectportal.firefox.com/success.txt"))
                startActivity(myIntent)
            }
            LoginResponse.ALREADY_LOGGED_IN -> {
                notifyUser("Error!", "You're already logged into the WiFi! Try again when you're not", context)
            }
            else -> {
                notifyUser("Error!", "Something unknown happened to your login request! Try double checking your username and password", context)
            }
        }

        //Afterwards load ad (when we have internet)
        loadAd()
    }

    override fun onPause() {
        super.onPause()
        mRewardedVideoAd!!.pause(this)
    }

    override fun onResume() {
        super.onResume()
        mRewardedVideoAd!!.resume(this)

        if (this.findViewById<LinearLayout>(R.id.batteryOptimisations).visibility == View.VISIBLE && !batteryOptimisations()) {
            this.findViewById<LinearLayout>(R.id.batteryOptimisations).visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mRewardedVideoAd != null) {
            mRewardedVideoAd!!.destroy(this)
        }
    }

    private fun notifyUser(message: String, description: String, context: Context) {
        //main.sendTimedNotification(message, description, context, notificationTimeout)
        Toast.makeText(this, "$message $description", Toast.LENGTH_LONG).show()
    }

    fun loginAutomatically(view: View) {
        // Use an activity context to get the rewarded video instance.

        mRewardedVideoAd!!.show()
    }

    fun unlockPremium () {
        this.getSharedPreferences("credentials", 0).edit().putBoolean("premium", true).apply()

        Toast.makeText(this, "Unlocked automatic logins!", Toast.LENGTH_LONG).show()
        ScreenDetection.Setup(this)
        premiumButton!!.visibility = View.GONE
        this.findViewById<TextView>(R.id.premiumText)!!.visibility = View.VISIBLE

        if (batteryOptimisations()) {
            this.findViewById<LinearLayout>(R.id.batteryOptimisations).visibility = View.VISIBLE
        }
    }

    class AdRewardManager(main: MainActivity): RewardedVideoAdListener {

        var main: MainActivity = main

        override fun onRewardedVideoAdLeftApplication() {

        }

        override fun onRewardedVideoAdLoaded() {

        }

        override fun onRewardedVideoAdOpened() {

        }

        override fun onRewardedVideoCompleted() {

        }

        override fun onRewarded(p0: RewardItem?) {
            main.unlockPremium()
        }

        override fun onRewardedVideoStarted() {

        }

        override fun onRewardedVideoAdFailedToLoad(p0: Int) {

        }

        override fun onRewardedVideoAdClosed() {
            main.mRewardedVideoAd!!.loadAd("ca-app-pub-5689777634096933/1702403608", AdRequest.Builder().build())
        }

    }
}
